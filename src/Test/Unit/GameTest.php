<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . "/../../../vendor/autoload.php";
require __DIR__ . "/../../Entity/Game.php";

class GameTest extends TestCase {

  public function testImage_WithNull_ReturnPlaceholder() {
    $game = new Game();
    $game->setImagePath(NULL);
    $this->assertEquals('/images/placeholder.jpg', $game->getImagePath());
  }

  public function testIsRecommended_With5_ReturnsTrue() {
    // Only overwrite getAverageScore method.
    $game = $this->createPartialMock('Game', ['getAverageScore']);
    $game->method('getAverageScore')
      ->willReturn(5);

    $this->assertTrue($game->isRecommended());
    $this->assertEquals('5', $game->getAverageScore());
  }

  public function testAverageScore_WithoutRatings_ReturnsNull() {
    $game = new Game();
    $game->setRating([]);
    $this->assertNull($game->getAverageScore());
  }

  public function testAverageScore_With6And8_Returns7() {

    $rating1 = $this->getMockBuilder('Rating')->setMethods(array('getScore'))->getMock();
    $rating1->method('getScore')
      ->willReturn(6);

    $rating2 = $this->getMockBuilder('Rating')->setMethods(array('getScore'))->getMock();
    $rating2->method('getScore')
      ->willReturn(8);

    $game = $this->createPartialMock('Game', ['getRatings']);
    $game->method('getRatings')
      ->willReturn([$rating1, $rating2]);

    $this->assertEquals(7, $game->getAverageScore());
  }

  public function testAverageScore_WithNullAnd5_Returns5() {

    $rating1 = $this->getMockBuilder('Rating')->setMethods(array('getScore'))->getMock();
    $rating1->method('getScore')
      ->willReturn(NULL);

    $rating2 = $this->getMockBuilder('Rating')->setMethods(array('getScore'))->getMock();
    $rating2->method('getScore')
      ->willReturn(5);

    $game = $this->createPartialMock('Game', ['getRatings']);
    $game->method('getRatings')
      ->willReturn([$rating1, $rating2]);

    $this->assertEquals(5, $game->getAverageScore());
  }

}
