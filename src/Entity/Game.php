<?php

class Game {
  protected $title;
  protected $imagePath;
  protected $ratings;

  public function getAverageScore() {
    $ratings = $this->getRatings();
    $numRatings = count($this->getRatings());

    if ($numRatings == 0) {
      return NULL;
    }

    $total = 0;
    foreach ($ratings as $rating) {
      $score = $rating->getScore();
      if ($score === NULL) {
        $numRatings--;
        continue;
      }
      $total += $rating->getScore();
    }

    return $total / $numRatings;
  }

  public function isRecommended() {
    return $this->getAverageScore() >= 3;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getImagePath() {
    if ($this->imagePath == NULL) {
      return '/images/placeholder.jpg';
    }
    return $this->imagePath;
  }

  public function getRatings() {
    return $this->ratings;
  }

  public function setTitle($title) {
    $this->title = $title;
  }

  public function setImagePath($path) {
    $this->imagePath = $path;
  }

  public function setRating($rating) {
    $this->ratings = $rating;
  }

}